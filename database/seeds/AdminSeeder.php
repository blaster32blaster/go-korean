<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'olaf@webnician.net',
            'password' => bcrypt('AA15*rtM'),
        ]);

        DB::table('users')->insert([
            'name' => 'SubAdmin',
            'email' => 's.broms@outlook.com',
            'password' => bcrypt('BB15*rtM'),
        ]);
    }
}
