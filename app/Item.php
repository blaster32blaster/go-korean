<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property mixed $questions
 * @property mixed $answers
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 */
class Item extends Model
{
    protected $fillable = [
        'description',
        'text_content',
        'file_content',
        'image_content'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions() : BelongsToMany
    {
        return $this->belongsToMany('App\Question', 'question_item');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function answers() : BelongsToMany
    {
        return $this->belongsToMany('App\Answer', 'item_answers');
    }
}
