<?php

namespace App\Http\Services;

use App\Http\Resources\QuestionResource;
use App\Http\Resources\QuestionsResource;
use App\Question;

/**
 * Class QuestionsService
 * @package App\Http\Services
 */
class QuestionsService
{
    /**
     * @return QuestionsResource
     */
    public function index()
    {
        return new QuestionsResource(Question::all());
    }

    /**
     * @param $id
     * @return QuestionResource
     */
    public function show($id)
    {
        return new QuestionResource(Question::find($id));
    }
}