<?php

namespace App\Http\Services;

use App\Item;
use App\Question;
use App\Http\Resources\ItemsResource;

/**
 * Class QuestionItemService
 * @package App\Http\Services
 */
class QuestionItemService
{
    /**
     * @return ItemsResource
     */
    public function index()
    {
        return new ItemsResource(Item::all());
    }

    /**
     * @param $id
     * @return ItemsResource
     */
    public function show($id)
    {
        return new ItemsResource(Question::find($id)->items);
    }
}