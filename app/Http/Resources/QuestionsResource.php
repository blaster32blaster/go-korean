<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class QuestionsResource
 * @package App\Http\Resources
 */
class QuestionsResource extends ResourceCollection
{
    /**
     * @var string $collects
     */
    public $collects = '\App\Http\Resources\QuestionResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        @todo : set this up for pagination
        return [
            'data' => $this->collection,
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
