<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ItemResource
 * @package App\Http\Resources
 */
class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'description' => $this->description,
          'textContent' => $this->text_content,
          'fileContent' => $this->file_content,
          'imageContent' => $this->image_content,
          'created_at' => $this->created_at,
          'updated_at' => $this->updated_at
      ];
    }
}
