<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class ItemsResource
 * @package App\Http\Resources
 */
class ItemsResource extends ResourceCollection
{
    /**
     * @var string $collects
     */
    public $collects = '\App\Http\Resources\ItemResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        @todo : set this up for pagination
        return [
            'data' => $this->collection,
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
