<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property mixed $items
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 */
class Answer extends Model
{
    protected $fillable = [
        'description',
        'text_content',
        'file_content',
        'image_content'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function items() : BelongsToMany
    {
        return $this->belongsToMany('App\Item', 'item_answers');
    }
}
