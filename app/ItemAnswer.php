<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ItemAnswer extends Pivot
{
//    so weve got some tables that should serve the purpose
//    @todo : 1. set the model relationships up
//    @todo : 2. test models and relationships to ensure that they will serve properly
//    @todo : 3. build the admin UI

    public $incrementing = true;

    protected $fillable = [
        'item_id',
        'answer_id',
        'correct'
    ];
}
