<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $items
 */
class Question extends Model
{
    protected $fillable = [
        'description',
        'text'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function items()
    {
        return $this->belongsToMany('App\Item', 'question_item');
    }

//    @todo : need to set up a polymorphic relationship with difficulties
}
